## Overview #

This project is my entry for PyWeek 21 - The Aftermath.

The plan is to make a weird ren'py game with 3rd-party art.

Planned minigames:

* Darts with Loki
* Climb the tree, dodge squirrels
* beach volleyball with mermaids!

`game` contains the ren'py source for the main game

`homemade` contains a a test of displaying 2D images via OpenGL from within pygame. However, I don't think I
can call OpenGL from within ren'py.
