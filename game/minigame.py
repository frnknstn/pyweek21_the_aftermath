import time

try:
    import renpy.store as store
    import renpy.exports as renpy
except ImportError:
    # appease pycharm
    store = renpy = None
    raise


class Minigame(renpy.Displayable):
    """Helper class for a ren'py minigame.

    How to use:

    * subclass Minigame
    * implement do_init(...) to handle setting up your game
    * implement do_frame(display, width, height, st, at)
    ** draw your frame to the supplied 'display' object
    ** when your game is won or lost, put the results in a dict or whatever and call .terminate(results)
    * implement do_event(event, x, y, st)

    """

    def __init__(self, *args, **kwargs):
        super(Minigame, self).__init__(*args, **kwargs)

        self.do_init(*args, **kwargs)

        self._running = True
        self._frame_count = 0
        self._start_time = time.clock()
        self._end_time = None
        self._results = None

    def render(self, width, height, st, at):
        screen = renpy.Render(width, height)

        self.do_render(screen, width, height, st, at)

        self._frame_count += 1
        if self._running:
            renpy.redraw(self, 0)

        return screen

    def terminate(self, results):
        """Stop the game at the end of the frame, and send the result object to anyone who asks for it"""
        self._results = results
        self._running = False
        self._end_time = time.clock()

        # force an event so that renpy notices we are done
        renpy.timeout(0)

    def event(self, ev, x, y, st):
        if self._running:
            self.do_event(ev, x, y, st)
            return None
        else:
            return self._results

    def get_duration(self):
        if self._end_time is not None:
            end_time = self._end_time
        else:
            end_time = time.clock()
        return end_time - self._start_time

    # the following classes must be overridden by any children
    def do_init(self, *args, **kwargs):
        raise NotImplementedError("method do_init() must be overridden in a subclass")

    def do_render(self, display, width, height, st, at):
        raise NotImplementedError("method do_render() must be overridden in a subclass")

    def do_event(self, event, x, y, st):
        raise NotImplementedError("method do_event() must be overridden in a subclass")
