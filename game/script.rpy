﻿# You can place the script of your game in this file.


init:
    
    python:
        import time

        from minigame import Minigame
        
        
        class AwesomeBox(Minigame):
            def do_init(self):
                pass
            
            def do_render(self, screen, width, height, st, at):
                time.sleep(0.001)
                
                if self._frame_count >= 30:
                    duration = self.get_duration()
                    results = {
                        "duration": duration,
                        "frame_count": self._frame_count,
                        "fps": str("%0.3f" % (self._frame_count / duration))
                    }
                    self.terminate(results)
                
                return screen

            def do_event(self, ev, x, y, st):
                pass

# Declare background scenery here
# eg. image bg school = "school.jpg"

image bg queens_arms = "bg_queens_arms.jpg"

define fade_to_black = Fade(0.5, 0.2, 0.5)

# Declare images below this line, using the image statement.
# eg. image eileen happy = "eileen_happy.png"

image dan neutral = "man_neutral.png"
image dan sad = "man_sad.png"
image dan happy = "man_happy.png"
image dan mad = "man_mad.png"

# Declare characters used by this game.
define e = Character('Eileen', color="#ff3004")

define dan = Character('Dan', color="#757575")


# The game starts here.
label start:

    scene bg queens_arms

    show dan neutral
    dan "Hello! I got the van!"
    
    show dan happy at left
    dan "This is my happy face!"
    
    show dan mad at right
    dan "This is my *ANGRY* face!"
    
    show dan sad at center
    dan "This is my sad face :("
    
    with fade_to_black

    e "Hello, world! Welcome to my game! Let me test your framerate..."
    
    jump test_framerate
    
label test_framerate:
    python:
        ui.add(AwesomeBox())
        messages = ui.interact()
    
    e "Hmm... [messages[frame_count]] frames in [messages[duration]] seconds... That's [messages[fps]] FPS!"
    e "Would you like me to run the test again?"
    
    menu:
        "Yes, run the test again.":
            jump test_framerate
        "No thanks.":
            pass
            
    return
