#!/usr/bin/python2
from __future__ import division
from __future__ import print_function

import time

import pygame
from pygame.locals import *
import OpenGL
from OpenGL.GL import *
from OpenGL.GLU import *

SCREEN_SIZE = (1366, 768)


class FPSCounter(object):
    def __init__(self):
        self.last_frame = time.clock()

    def tick(self):
        current_time = time.clock()
        delta = current_time - self.last_frame
        self.last_frame = current_time
        return 1 / delta


def init_screen():
    pygame.init()
    screen = pygame.display.set_mode(SCREEN_SIZE, HWSURFACE|OPENGL|DOUBLEBUF)
    pygame.display.set_caption("PyWeek 21 - The Aftermath")
    return screen

def init_opengl():
    glViewport(0, 0, SCREEN_SIZE[0], SCREEN_SIZE[1]);

    glEnable(GL_TEXTURE_2D);

    glClearColor(0.2, 0.4, 0.0, 1.0)
    glClear(GL_COLOR_BUFFER_BIT)

    glMatrixMode(GL_TEXTURE);
    glLoadIdentity();

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()

    # gluPerspective(45.0, SCREEN_SIZE[0] / SCREEN_SIZE[1], 0.1, 100.0)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    print("OpenGL set up")


def draw_nehe():
    # draw the example crap I found in that nehe tut
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    glLoadIdentity()					# Reset The View

    # Move Left 1.5 units and into the screen 6.0 units.
    glTranslatef(-1.5, 0.0, -6.0)

    # Draw a triangle
    glBegin(GL_POLYGON)                 # Start drawing a polygon
    glVertex3f(0.0, 1.0, 0.0)           # Top
    glVertex3f(1.0, -1.0, 0.0)          # Bottom Right
    glVertex3f(-1.0, -1.0, 0.0)         # Bottom Left
    glEnd()                             # We are done with the polygon

    # Move Right 3.0 units.
    glTranslatef(3.0, 0.0, 0.0)

    # Draw a square (quadrilateral)
    glBegin(GL_QUADS)                   # Start drawing a 4 sided polygon
    glVertex3f(-1.0, 1.0, 0.0)          # Top Left
    glVertex3f(1.0, 1.0, 0.0)           # Top Right
    glVertex3f(1.0, -1.0, 0.0)          # Bottom Right
    glVertex3f(-1.0, -1.0, 0.0)         # Bottom Left
    glEnd()                             # We are done with the polygon


def main():
    screen = init_screen()
    init_opengl()
    fps = FPSCounter()

    # prettyyyyyy
    pretty_pic = pygame.image.load("shiny.bmp").convert()

    # make a screen texture
    screen_texture_handle = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, screen_texture_handle)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP)
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE)
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
                 pretty_pic.get_width(), pretty_pic.get_height(),
                 0, GL_RGBA, GL_UNSIGNED_BYTE,
                 pygame.image.tostring(pretty_pic, "RGBA", True))



    running = True
    while running:


        # process events:
        for event in pygame.event.get():

            # exit events
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key in (pygame.K_ESCAPE, pygame.K_q):
                    running = False

        # draw_nehe()


        # draw the display
        glClear(GL_COLOR_BUFFER_BIT)

        glBegin(GL_TRIANGLES)

        P = 0.8

        # NOTE(casey): Lower triangle
        glTexCoord2f(0.0, 0.0)
        glVertex2f(-P, -P)

        glTexCoord2f(1.0, 0.0)
        glVertex2f(P, -P)

        glTexCoord2f(1.0, 1.0)
        glVertex2f(P, P)

        # NOTE(casey): Upper triangle
        glTexCoord2f(0.0, 0.0)
        glVertex2f(-P, -P)

        glTexCoord2f(1.0, 1.0)
        glVertex2f(P, P)

        glTexCoord2f(0.0, 1.0)
        glVertex2f(-P, P)

        glEnd()

        # present the frame
        pygame.display.flip()
        frame_rate = fps.tick()
        time.sleep(0.001)
        print(frame_rate)



if __name__ == "__main__":
    try:
        main()
    finally:
        pygame.quit()

    print("Done")

